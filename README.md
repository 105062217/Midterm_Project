# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Forum]
* Key functions (add/delete)
    1. [user page]
    2. [post page]
    3. [post list page]
    4. [leave comment under any post]
* Other functions (add/delete)
    1. [編輯自己曾經發過的貼文，不能更改別人的]
    2. [當自己的貼文被回應，會收到通知]
    3. [發文時間]
    4. [跳動標題]
    5. [若是貼文輸入有換行，保留換行格式，會防範輸入框沒有打字的情況]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description

* 1. URL: https://105062217.gitlab.io/Midterm_Project/
* 2. Firebase host: https://simplefourm-d450c.firebaseapp.com/
* 3. Sign-in: 提供兩種登入方式，信箱註冊、google登入。登入後上方的navigation bar會有登出的選項，並且會顯示使用者信箱。
* 4. Profile: 登入才能得到使用者資料。記錄 Email、UID、性別、生日。
* 5. UID: 論壇中發文以及回文都會使用UID來表示、方便識讀。
* 6. 新帳號: 每當註冊新帳號，會被要求設定Profile。
* 7. 回文: 每篇文章的標題都是連結，點進去可以看到回應並發表新回應、以及編輯貼文。
* 8. 編輯: 只能編輯自己發布的文章，編輯完後會自動更新。
* 9. 發文: 發布文章時記得連同標題及內文都要打。
* 10. 一鍵到頂、一鍵到底: 右上角有按鍵可以到最頂或最底，方便閱讀。
* 11. RWD: 支援自適應介面，背景圖以cover的方式呈現，不怕變形。
* 12. 通知: 當自己的貼文被回應，會收到通知，點擊通知可直接到那個頁面。


## Security Report (Optional)
