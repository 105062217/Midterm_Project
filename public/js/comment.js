function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        notify(user);
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='profile-btn'>Profile" + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logout_button = document.getElementById('logout-btn');
            var profile_button = document.getElementById('profile-btn');
            profile_button.addEventListener('click', function () {
                window.location.href = "profile.html";
            });
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!');
                        window.location.href = "index.html";
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!');
                    });
            });
            var posterRef = firebase.database().ref('user_list/'+user.uid);
            
            posterRef.once('value').then(function (snapshot) {
                userData = snapshot.val();
                if(!snapshot.exists()){
                    var posterRef = firebase.database().ref('user_list/'+user.uid);
                    posterRef.set("");
                    alert("Please update your profile");
                }
            });

            var edit = document.getElementById('edit_btn');
            edit.addEventListener('click', function () {
                window.location.href = "edit.html?" + ArticleID;
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    var today = new Date();
    var year = today.getFullYear();
    var month = today.getMonth() + 1;
    var day = today.getDate();
    var time = year + "年" + month + "月" + day + "日";

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var user = firebase.auth().currentUser;
            var Ref = firebase.database().ref('com_list/'+ ArticleID);
            var posterRef = firebase.database().ref('user_list/'+ user.uid);
            posterRef.once('value').then(function (snapshot) {
                var articleData = snapshot.val();
                firebase.database().ref('com_list/'+ ArticleID + '/response_list').push({
                    email: user_email,
                    data: "<pre>" + post_txt.value + "</pre>",
                    time: time,
                    UID: articleData.UID
                });
                post_txt.value = "";
                alert("Successfully response");
            });

        }
    });

    var ArticleID = location.search.substring(1, location.search.length);

    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_before_username1 = "</h6><div class='media text-muted pt-3'><img src='https://vignette.wikia.nocookie.net/tos/images/d/d4/Stone.png/revision/latest?cb=20150611063725&path-prefix=zh' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var ArticleRef = firebase.database().ref('com_list/'+ ArticleID);
    var Title = document.getElementById('Title');
    var Article = document.getElementById('Article');
    var Author = document.getElementById('Author');
    var Article_time = document.getElementById('Article_time');

    ArticleRef.once('value').then(function (snapshot) {
        var userData = snapshot.val();
        Title.innerHTML += " " + userData.title;
        Article.innerHTML += " " + userData.data;
        Author.innerHTML += " " + userData.UID;
        Article_time.innerHTML += " " + userData.time;
    });

    var postsRef = firebase.database().ref('com_list/'+ ArticleID + '/response_list');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                total_post[total_post.length] = str_before_username + "Re: " + str_before_username1 + "By : " + childData.UID + "</br> Summit date: "+ childData.time + "</strong></div>" +
                                                "<div><h6></h6>" + childData.data + "</div>" + str_after_content;
                first_count += 1;
            });

            document.getElementById('post_list').innerHTML = total_post.join('');

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    
                    total_post[total_post.length] = str_before_username + "Re: " + str_before_username1 + "By : " + childData.UID + "</br> Summit date: "+ childData.time + "</strong></div>" +
                                                    "<div><h6></h6>" + childData.data + "</div>" + str_after_content;
                                    
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
}