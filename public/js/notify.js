function notify(user){
    if(Notification.permission === "default"){
        Notification.requestPermission();
    }
    else if(Notification.permission === "granted"){
        var postsref = firebase.database().ref('com_list');
        
        postsref.once('value').then(function (snapshot) {
            snapshot.forEach(function(childshot) {
                var data = childshot.val();
                var articleref = firebase.database().ref('com_list' + childshot.key);
                var commentref = firebase.database().ref('com_list/' + childshot.key + '/response_list');

                var first_count = 0;
                var second_count = 0;

                commentref.once('value').then(function (snapshot) {
                    snapshot.forEach(function() { first_count += 1; });
                    
                    commentref.on('child_added', function (addshot) {
                        second_count += 1;
                        if(second_count > first_count){
                            var childData = addshot.val();

                            if(user.email == data.email){
                                var link = "comment.html?" + childshot.key;
                                var notification = new Notification("您的貼文有新回應",{
                                    icon: 'http://1.blog.xuite.net/1/b/c/5/243866999/blog_4525566/txt/318836046/7.png',
                                    body: "Re : " + data.title
                                });
                                notification.onclick = function(){
                                    window.open(link);
                                    notification.close();
                                }
                            }
                        }
                    });
                });
            });
        });

    }
}


