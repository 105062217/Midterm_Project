function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        notify(user);
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='profile-btn'>Profile" + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logout_button = document.getElementById('logout-btn');
            var profile_button = document.getElementById('profile-btn');
            var profile_email = document.getElementById('profile_email');
            profile_email.innerHTML += " " + user.email;
            profile_button.addEventListener('click', function () {
                window.location.href = "profile.html";
            });
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!');
                        window.location.href = "index.html";
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!');
                    });
            });
            var UID = document.getElementById('UID');
            var set = document.getElementById('set');
            var birthday = document.getElementById('birthday');
            var sex = document.getElementById('sex');
            var posterRef = firebase.database().ref('user_list/'+ user.uid);
            var profile_UID = document.getElementById('profile_UID');
            var profile_birthday = document.getElementById('profile_birthday');
            var profile_sex = document.getElementById('profile_sex');

            posterRef.once('value').then(function (snapshot) {
                var userData = snapshot.val();
                profile_sex.innerHTML += " " + userData.sex;
                profile_birthday.innerHTML += " " + userData.birthday;
                profile_UID.innerHTML += " " + userData.UID;

                set.addEventListener('click', function () {
                    var userData = {
                        UID: UID.value,
                        birthday: birthday.value,
                        sex: sex.value
                    }
                    posterRef.set(userData);
                    alert("Set completely");
                    location.reload();
                });
                UID.value = userData.UID;
                birthday.value = userData.birthday;
                sex.value = userData.sex;
            });
            
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

}

window.onload = function () {
    init();
}